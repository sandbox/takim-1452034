<?php
// $Id: payson.admin.inc,v 1.2 2010/09/11 19:09:34 mikl Exp $

/**
 * @file
 * Administration pages for payson module.
 */

/**
 * Administration settings form.
 */
function payson_admin_settings_form(&$form_state) {
  $form = array();

 

   $form['payson_receiver_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Receiver Email'),
    //'#description' => t('payson merchant account number. Can be found under “Merchant Profile” in the payson admin panel.'),
    '#default_value' => variable_get('payson_receiver_email',''),
    '#required' => TRUE,
  );


  $form['payson_agent_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Agent account'),
    //'#description' => t('payson merchant account number. Can be found under “Merchant Profile” in the payson admin panel.'),
    '#default_value' => variable_get('payson_agent_id',''),
    '#required' => TRUE,
  );

 
    $form['payson_md5_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Md5 key'),
      '#default_value' => variable_get('payson_md5_key', ''),
     // '#description' => t('payson encryption key. You can generate a new encryption under “Merchant Profile” in the payson admin panel. If you do, remember to change it here as well, or all payments will fail.'),
      '#required' => TRUE,
    );

   

  $form['payson_currencyCode'] = array(
    '#type' => 'select',
    '#title' => t('Default currency'),
    //'#description' => t('Select the default currency for payson payments. This setting may be overridden by implemeting modules.'),
    '#required' => TRUE,
    '#options' => array(
       'EUR' => t('Euro'),
       'SEK' => t('Swedish krona'),
    ),
     '#default_value' => variable_get('payson_currencyCode', 'SEK'),
  );

  $form['payson_localeCode'] = array(
    '#type' => 'select',
    '#title' => t('Locale'),
    //'#description' => t('Select the default currency for payson payments. This setting may be overridden by implemeting modules.'),
    '#required' => TRUE,
    '#options' => array(
       'SV' => t('Swedish'),
       'EN' => t('English'),
    ),
     '#default_value' => variable_get('payson_localeCode', 'EN'),
  );

 $form['payson_taxPercentage'] = array(
    '#type' => 'textfield',
    '#title' => t('Tax value'),
    //'#description' => t('payson merchant account number. Can be found under “Merchant Profile” in the payson admin panel.'),
    '#default_value' => variable_get('payson_taxPercentage','0.25'),
    '#required' => TRUE,
  );

 $form['payson_extracost'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra Cost'),
    //'#description' => t('payson merchant account number. Can be found under “Merchant Profile” in the payson admin panel.'),
    '#default_value' => variable_get('payson_extracost','50'),
    '#required' => TRUE,
  );

 $form['array_filter'] = array('#type' => 'hidden');

    return system_settings_form($form);
}
